use bytes::Bytes;
use handlebars::Handlebars;
use rand::Rng;
use std::collections::{BTreeMap, HashMap};
use tungstenite::{connect, client::connect_with_config};
use url::Url;
use uuid::Uuid;
pub mod structs;
use structs::{ComfyUI, GenerationResponse, HistoryData};
pub mod tests;

impl ComfyUI {
    pub async fn get_history(
        self,
        promt_id: String,
    ) -> Result<HashMap<String, HistoryData>, serde_json::Error> {
        let client = reqwest::Client::new();

        let res = client
            .get(format!("{}/history/{}", self.format_url(true), promt_id))
            .send()
            .await
            .unwrap();
        let data: Result<HashMap<String, HistoryData>, serde_json::Error> =
            serde_json::from_str(&res.text().await.unwrap());
        if let Ok(obj) = data {
            return Ok(obj);
        } else {
            return Err(data.err().unwrap());
        }
    }
    pub async fn get_image(
        self,
        filename: &String,
        subfolder: &String,
        folder_type: &String,
    ) -> Bytes {
        let client = reqwest::Client::new();
        let res = client
            .get(format!(
                "{}/view?filename={}&subfolder={}&type={}",
                self.format_url(true),
                filename,
                subfolder,
                folder_type
            ))
            .send()
            .await
            .unwrap();
        //write to tmp file
        res.bytes().await.unwrap()
        // return res.bytes().await.unwrap();
    }
    pub async fn send(self, json: String) -> Result<Bytes, String> {
        let client = reqwest::Client::new();
        println!("client id: {}", self.client_id);
        let res = client
            .post(format!("{}/prompt", &self.clone().format_url(true)))
            .body(json)
            .send()
            .await;
        if let Ok(resp) = res {
            // println!("{:?}", resp.text().await);
            // panic!("hi");
            let resp: Result<GenerationResponse, serde_json::Error> =
                serde_json::from_str(&resp.text().await.unwrap());

            if let Ok(json) = resp {
                //open websocket
                let id = json.prompt_id;
                println!("promt id: {}", id);
                let url = Url::parse(&format!(
                    "{}{}:{}/ws?clientId={}",
                    self.web_socket_protocol, self.url, self.port, self.client_id
                ))
                .unwrap();
                let mut client_config = None;
                if !(url.scheme() == "ws" || url.scheme() == "wss") {
                    panic!("Invalid URL scheme for WebSocket: {}", url);
                }
                else {
                    println!("scheme: {}", url.scheme())
                }
                let (mut socket, response) = connect_with_config(url, client_config,1).expect("Can't connect");

                loop {
                    //imagine it failing and waiting forever. that is what this code does pls fix that
                    let msg = socket.read().unwrap().to_string();
                    println!("msg: {}", msg);
                    let success = format!("{{\"type\": \"executed");
                    if msg.contains(&success) {

                        if msg.contains(&id) {
                            let history = self.clone().get_history(id.clone()).await;
                            if let Ok(h) = history {
                                let outputs = &h.get(&id).unwrap().outputs;
                                let image = &outputs.into_iter().next().unwrap().1.images.first();
                                if let Some(i) = image {
                                    let image = self
                                        .clone()
                                        .get_image(&i.filename, &i.subfolder, &i.image_type)
                                        .await;
                                    return Ok(image);
                                }
                            }
                        }
                    }
                    let fail = format!(
                        "{{\"type\": \"execution_error\", \"data\": {{\"prompt_id\": \"{}\"",
                        id
                    );
                    if msg.contains(&fail) {
                        return Err("generation failed".to_string());
                    }
                }
            } else {
                return Err("Server returned something we did not understand".to_string());
            }
        } else {
            return Err("could not connect to server".to_string());
        }
    }
    pub async fn simple_promt(
        self,
        positive: String,
        negative: String,
        model: String,
        seed: Option<u32>,
    ) -> Result<Bytes, String> {
        let mut handlebars = Handlebars::new();
        let mut data = BTreeMap::new();

        let contents = include_str!("templates/simple.json.hbs");
        if let Some(s) = seed {
            data.insert("seed".to_string(), s.to_string());
        } else {
            let mut rng = rand::thread_rng();
            // TODO: generate a random number here
            data.insert("seed".to_string(), rng.gen::<u32>().to_string());
        }
        if handlebars.register_template_string("t1", contents).is_ok() {
            data.insert("model".to_string(), model);
            data.insert("positive".to_string(), positive);
            data.insert("negative".to_string(), negative);
            data.insert("client_id".to_string(), self.client_id.to_string());
            let rendered = handlebars.render("t1", &data).unwrap();
            let response = self.send(rendered).await;
            return response;
        }
        Err("Failed to render json".to_string())
    }

    pub async fn lora_promt(
        self,
        positive: String,
        negative: String,
        lora_1: String,
        lora_2: String,
        lora_3: String,
        model: String,
        seed: Option<u32>,
    ) -> Result<Bytes, String> {
        let mut handlebars = Handlebars::new();
        let mut data = BTreeMap::new();

        let contents = include_str!("templates/lora.json.hbs");
        if let Some(s) = seed {
            data.insert("seed".to_string(), s.to_string());
        } else {
            let mut rng = rand::thread_rng();
            // TODO: generate a random number here
            data.insert("seed".to_string(), rng.gen::<u32>().to_string());
        }
        if handlebars.register_template_string("t1", contents).is_ok() {
            data.insert("model".to_string(), model);
            data.insert("positive".to_string(), positive);
            data.insert("negative".to_string(), negative);
            data.insert("lora_name_1".to_string(), lora_1);
            data.insert("lora_name_2".to_string(), lora_2);
            data.insert("lora_name_3".to_string(), lora_3);
            data.insert("client_id".to_string(), self.client_id.to_string());
            let rendered = handlebars.render("t1", &data).unwrap();
            let response = self.send(rendered).await;
            return response;
        }
        Err("Failed to render json".to_string())
    }
}
impl Default for ComfyUI {
    fn default() -> Self {
        ComfyUI {
            client_id: Uuid::new_v4(),
            protocol: "http://".to_string(),
            web_socket_protocol: "ws://".to_string(),
            url: "localhost".to_string(),
            port: 8188,
        }
    }
}
impl ComfyUI {
    fn ssl() -> Self {
        ComfyUI {
            client_id: Uuid::new_v4(),
            protocol: "https://".to_string(),
            web_socket_protocol: "wss://".to_string(),
            url: "localhost".to_string(),
            port: 443,
        }
    }
}
