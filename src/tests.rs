use std::{fs::File, io::Write};

use crate::structs::ComfyUI;
#[tokio::test]
async fn test_simple_promt() {
    let comfyUI = ComfyUI::default();
    let image = comfyUI
        .simple_promt(
            "a dog sitting on a bench".to_string(),
            "text, watermark embedding:EasyNegative, embedding:FastNegativeV2, embedding:ng_deepnegative_v1_75t, embedding:verybadimagenegative_v1.3,".to_string(),
            "dreamshaper_8.safetensors".to_string(),
            None,
        )
        .await;
    if let Ok(i) = image {
        let mut file = File::create("test/simple.png").unwrap();
        file.write_all(&i);
        
    }
    else {
        panic!("{}", image.is_err());
    }
}

#[tokio::test]
async fn test_lora_promt() {
    let comfyUI = ComfyUI::default();
    let image = comfyUI.lora_promt("a dog sitting on a bench".to_string(),
    "text, watermark embedding:EasyNegative, embedding:FastNegativeV2, embedding:ng_deepnegative_v1_75t, embedding:verybadimagenegative_v1.3,".to_string(),
    "None".to_string(),
    "None".to_string(),
    "None".to_string(),
    "dreamshaper_8.safetensors".to_string(), 
     None
    ).await;
    if let Ok(i) = image {
        let mut file = File::create("test/lora.png").unwrap();
        file.write_all(&i);
        
    }
    else {
        panic!("{}", image.is_err());
    }
}
